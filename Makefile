recovery_reboot:
	$(CC) -I. recovery_reboot.c -o recovery_reboot

install:
	install -m 755 recovery_reboot $(DESTDIR)/usr/sbin

clean:
	-rm recovery_reboot

all: recovery_reboot
